# written in Python 3

from collections import Counter
from math import gcd
from itertools import tee, islice, chain
import re, sys

# future improvement - make this a class
# cryptogram for solving = 'IGGKPYSRBCIZIHGSQIRFOCYKIHJRXBEJQWOJBUXVACZKQHMZVUYKZYKKACJVKULVEUZTPCTXXYUGTYMFQHMZVUTUKISZVAULBILRPIAJMITKPYUKPYXJQXKFNNNVANXVMNLZZMZKPYEJMYZNWJKFXFKXWCTXQHZFBBKYWOYVBCSVXUYJMMGWBYXREBOCMNNVGHUKQWKKPLKVXYXJWHYTWGOEOIAKWZZYMBULAYZYMJNPACIZANYRGMZYMCTZBCGCUYGJCLKDMHZNIMTKIWILZUZVBBKSQIRFOCYKAUEJBBKPPUBVZYVIWXATMXZYMGGKPYSRBCIZIHYRGMOWMRGTBFEFVYVVZMUEMHZVZMZYMBULAYZYMHOKECRCJYKDXNEROUOEEBKEPYTIGEOJACTXMLRVNNNRZPGILUTUEYTKBICRABOEONUEBIYVZPKZVNNVVCDFVUJDQHOJBLGKQITYMQGJIMQVLVEFVYNZAHKNKIRCMUMLMMGSWOZKPYVFTNOTIFOENCMYBCTXQHGTIXKDQUOEEUYYQHMKWHCVZYLRUIAJNIXGWFOKQWGCQHZIQAAVQNYFCLPFJMUDMITVIMQVLVAKEYXVXCQVZMIFUJGIMXZFBBKSIWQJBUHSQHMRVXJZZNEGWFOKQWYRBOTZDYXJQNOVAQNPLIEFCJKFXFKWQANKTCQVBBGKSCYJQHMVZCYJICJKWBGMMLKJXITUMXOEPCYCWQMIIPKCTSBFQWKZBMHVKUAJMNNVANGBMMGIMMUCWQ'
# ENGLISH_ALPHABET = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
# most common letter = 'E'

# finds offsets of each recurring triplet
def find(text, char):
    return [n for n in range(len(text)) if text.find(char, n) == n]

def create_triplets(ciphertext, initial_index):
    triplets_list = []
    triplets_offsets = {}
    diff = []
    while True:
        triplet = ciphertext[initial_index:initial_index+3]
        if triplet != '' and len(triplet) == 3:
            triplets_list.append(triplet)
            initial_index += 3
        else:
            break   
    for item in triplets_list:
        diff = []
        offsets = find(ciphertext, item)
        if len(offsets) > 2: 
            for offset in offsets[1:]:
                diff.append(offset - offsets[0])
            triplets_offsets[item] = offsets, diff
    print('\n[*] Repetitive triplets (and their offsets):\n%s' % triplets_offsets)
    return triplets_offsets

def get_current_and_next_value_in_list(offset_diff):
    items, nexts = tee(offset_diff, 2)
    nexts = chain(islice(nexts, 1, None), [None])
    return zip(items, nexts)

def gcd_of_triplet_offset_differences(ciphertext):
    gcd_of_diff = []
    for x in range(3):
        triplets = create_triplets(ciphertext, x)
        for key, value in triplets.items():
            for item, nxt in get_current_and_next_value_in_list(value[1]):
                if nxt:
                    gcd_of_diff.append(gcd(item, nxt))
    return gcd_of_diff

def get_key_len(gcd_of_diff):
    unique_values = []
    for num in gcd_of_diff:
        if num not in unique_values:
            unique_values.append(num)
    key_len = 0
    x = 0
    for num in unique_values:
        freq_of_gcd_diff = gcd_of_diff.count(num)
        if freq_of_gcd_diff > x:
            x = freq_of_gcd_diff
            key_len = num
    return key_len

def partial_text(ciphertext, initial_index, key_len):
    partial_text = ciphertext[initial_index]
    next_index = initial_index + key_len
    while next_index <= len(ciphertext)-1:
        next_letter = ciphertext[next_index]
        partial_text += next_letter
        next_index += key_len
    return partial_text

def get_partial_texts(ciphertext, key_len):
    partial_texts = []
    for iteration in range(key_len):
        partial_texts.append(partial_text(ciphertext, iteration, key_len))
    return partial_texts
  
def ic_calculation(partial_texts):
    subtext_ic = []
    for subtext in partial_texts:
        input_for_ic = '' 
        numerator = 0
        denominator = len(subtext) * (len(subtext) - 1)
        for letter in subtext:
            if letter not in input_for_ic:
                input_for_ic += letter
                letter_offsets = find(subtext, letter)
                freq = len(letter_offsets)
                numerator += freq * (freq - 1)
            else:
                continue
        subtext_ic.append(round(float(numerator / denominator), 3))
    return subtext_ic

def caesar(key_len, partial_texts):
    freq_list = []
    potential_e_letters = []
    for text in partial_texts:
        freq_dict = {}
        freq = Counter(text)
        for a in text:
            freq_dict[a] = freq[a]
        potential_e_letter = {}
        for c in sorted(freq_dict, key=freq_dict.get, reverse=True):
            potential_e_letter[c] = freq_dict[c]
        freq_list.append(freq_dict)
        potential_e_letter = list(potential_e_letter)[0] # take the most frequent letter from ordered dict - should be letter 'E'
        potential_e_letters.append(potential_e_letter)
    return potential_e_letters    

def find_key(alphabet, potential_e_letters, most_freq_letter):
    key = ''
    offset_e = find(alphabet, most_freq_letter)[0]
    for letter in potential_e_letters:
        shift_from_e = (find(alphabet, letter)[0] - offset_e) % len(alphabet) 
        key += alphabet[shift_from_e]
    return key

def get_plaintext(alphabet, key, ciphertext):
    plaintext = ''
    x = 0
    for letter in ciphertext:
        ciphertext_offset = find(alphabet, letter)[0]
        corresponding_key_letter_offset = find(alphabet, key[x % len(key)])[0]
        plaintext_offset = (ciphertext_offset - corresponding_key_letter_offset) % len(alphabet) 
        plaintext += alphabet[plaintext_offset]
        x += 1
    return plaintext
    
def main():
    ciphertext = input('[?] Enter ciphertext:\n').upper()
    if not re.match(r'^[a-zA-Z]+$', ciphertext):
        print('[-] Please enter valid ciphertext!')
        sys.exit(1)
    alphabet = input('[?] Enter alphabet:\n').upper()
    if not re.match(r'^[a-zA-Z]+$', alphabet):
        print('[-] Please enter valid alphabet!')
        sys.exit(1)
    most_freq_letter = input('[?] Enter most frequent letter in English alphabet:\n').upper()
    if not re.match(r'^[a-zA-Z]{1}$', most_freq_letter):
        print('[-] Please enter one English letter!')
        sys.exit(1)
    get_diff = gcd_of_triplet_offset_differences(ciphertext)
    key_len = get_key_len(get_diff)
    partial_texts = get_partial_texts(ciphertext, key_len)
    potential_e_letters = caesar(key_len, partial_texts)
    key = find_key(alphabet, potential_e_letters, most_freq_letter)
    plaintext = get_plaintext(alphabet, key, ciphertext)
    partial_texts_ic = ic_calculation(partial_texts)
    print('\n[*] Greatest common divisors of triplet offset differences:\n%s' % get_diff)
    print('\n[*] Most likely key length:', key_len)
    print('\n[*] Partial texts:\n%s' % partial_texts)
    print('\n[*] IC of partial texts:', partial_texts_ic)
    print('\n[*] Potential "E" letters in each partial text:', potential_e_letters)
    print('\n[+] Key: %s\n\n[+] Plaintext:\n%s' %(key, plaintext))
    pass

main()
